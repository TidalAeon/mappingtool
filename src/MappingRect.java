import java.awt.Graphics;

public class MappingRect {
    int x1, y1, x2, y2;

    MappingRect() {
    }

    MappingRect(MappingRect rect) {
        x1 = rect.x1;
        y1 = rect.y1;
        x2 = rect.x2;
        y2 = rect.y2;
    }

    void reset() {
        x1 = y1 = x2 = y2 = 0;
    }

    void draw(Graphics g) {
        int px = Math.min(x1, x2);
        int py = Math.min(y1, y2);
        int pw = Math.abs(x1 - x2);
        int ph = Math.abs(y1 - y2);
        g.drawRect(px, py, pw, ph);
    }
}