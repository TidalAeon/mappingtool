import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class WorkbenchPanel extends JLayeredPane{
    private final List<MappingRect> mappingRects = new ArrayList<>();
    private final MappingRect tempRect = new MappingRect();

    public boolean newImage = false;

    public WorkbenchPanel() {
        RectDrawerMouseListener listener = new RectDrawerMouseListener();
        addMouseListener(listener);
        addMouseMotionListener(listener);
        /*
        JLabel pic = new JLabel(
                new ImageIcon("/Users/anastasiarozanova/Downloads/2714 ОБРАБОТКА (16 of 21).jpg"));
        this.add(pic);
        */
    }


    private void setStartPoint(int x, int y) {
        tempRect.x1 = x;
        tempRect.y1 = y;
    }

    private void setEndPoint(int x, int y) {
        tempRect.x2 = x;
        tempRect.y2 = y;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.RED);

        // draw the current rect
        tempRect.draw(g);

        // draw the saved rects
        for (MappingRect mappingRect : mappingRects) {
            mappingRect.draw(g);
        }

        if (newImage){
            newImage = false;
        }
    }

    public class RectDrawerMouseListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            // reset the current rect because we want to make a new one
            tempRect.reset();
            setStartPoint(e.getX(), e.getY());
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            setEndPoint(e.getX(), e.getY());
            repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            setEndPoint(e.getX(), e.getY());
            // save the copy of the current rect to the list
            mappingRects.add(new MappingRect(tempRect));
            repaint();
        }
    }
}