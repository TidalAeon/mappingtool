import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class saveButton extends JButton implements ActionListener {
    public saveButton(){
        addActionListener(this);
    }
    public JLabel openedPic = null;

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        System.out.println("hello");
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Выбор скриншота");
        // Определение режима - файлы и каталоги
        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int result = fileChooser.showOpenDialog(this);
        System.out.println("Открыли окно и выбрали " + fileChooser.getSelectedFile());
        BufferedImage myPicture = null;
        try {
            myPicture = ImageIO.read(fileChooser.getSelectedFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("И уже приняли открытый файл и пытаемся поместить его в "+ Mapping.frame + " " + myPicture);
        openedPic = new JLabel(new ImageIcon(myPicture));
        openedPic.setPreferredSize(new Dimension(600, 520));
        Mapping.frame.setPreferredSize(openedPic.getSize());
        Mapping.frame.add(openedPic, BorderLayout.CENTER);
        Mapping.frame.repaint();
    }

}
