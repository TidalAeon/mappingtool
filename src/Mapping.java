import java.awt.*;
import java.io.File;
import javax.swing.*;

public class Mapping {
    public File scr = null;
    public static JFrame frame = null;
    public static WorkbenchPanel wbPanel = new WorkbenchPanel();

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame = new JFrame("Mapping"); // создали окно
                frame.setPreferredSize(new Dimension(600, 600));
                frame.setLayout(new BorderLayout());
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Закрыли окно - завершили программу
                frame.setVisible(true); // Сделали видимым

                setBgImage(new File("/Users/anastasiarozanova/Downloads/2714 ОБРАБОТКА (16 of 21).jpg"));
                //Рисуем панельку "Меню" с кнопками "Открыть" и "Сохранить"
                JPanel menu = new JPanel();

                //Добавляем кнопку "Открыть"
                openButton openButton = new openButton();
                openButton.setText("Открыть");
                menu.add(openButton);

                //Добавляем кнопку "Созранить"
                saveButton saveButton = new saveButton();
                saveButton.setText("Сохранить");
                menu.add(saveButton);

                frame.add(menu, BorderLayout.NORTH);

                //Рисуем новую панельку слева
                JPanel leftTextPanel = new JPanel();
                leftTextPanel.setVisible(true);
                leftTextPanel.setBackground(new Color(0,255,0));
                frame.add(leftTextPanel, BorderLayout.WEST);

                //Рисуем новую панельку справа
                JPanel rightTextPanel = new JPanel();
                rightTextPanel.setVisible(true);
                rightTextPanel.setBackground(new Color(255,0,0));
                frame.add(rightTextPanel, BorderLayout.EAST);

                frame.add(wbPanel, BorderLayout.CENTER);

                frame.pack();
            }
        });
    }

    public static void setBgImage (File bgImage){
        try {
            final Image backgroundImage = javax.imageio.ImageIO.read(
                    new File("/Users/anastasiarozanova/Downloads/2714 ОБРАБОТКА (16 of 21).jpg"));
            frame.setContentPane(new JPanel(new BorderLayout()) {
                @Override public void paintComponent(Graphics g) {
                    g.drawImage(backgroundImage, 0, 0, null);
                }
            });
        } catch (Exception e) {
        }
    }
}
